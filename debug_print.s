#define UART_BASE		0xE0001000
#define	UART_CHANNEL_STS_REG0	UART_BASE + 0x2C
#define	UART_TX_RX_FIFO0	UART_BASE + 0x30

.data
debug_text:
	.ascii "\r\n"
	.ascii "--------------------------------------------------------------------------\r\n"
	.ascii "r0  = 0x\0, r1  = 0x\0, r2  = 0x\0, r3  = 0x\0\r\n"
	.ascii "r4  = 0x\0, r5  = 0x\0, r6  = 0x\0, r7  = 0x\0\r\n"
	.ascii "r8  = 0x\0, r9  = 0x\0, r10 = 0x\0, r11 = 0x\0\r\n"
	.ascii "r12 = 0x\0, r13 = 0x\0, r14 = 0x\0, r15 = 0x\0\r\n"
	.ascii "cpsr = \0, \0, \0 mode, current mode = \0 ( = 0x\0 )\r\n"	@ NZCV, AIF, JT, M, hex
	.ascii "--------------------------------------------------------------------------\r\n"
	.byte 0x00

inst_mode_text:	.ascii "ARM\0Thumb\0Jazelle\0"	@ +0 = "ARM", +4 = "Thumb", +10 = "Jazelle"

proc_mode_text:	.ascii "USR\0FIQ\0IRQ\0SVC\0???\0???\0MON\0ABT\0"	@ 10xxx
		.ascii "???\0???\0HYP\0UND\0???\0???\0???\0SYS\0"	@ 11xxx

.text
# aliases for register
rx_uart_CH_STS    .req r10
rx_uart_TXRX_FIFO .req r9
rx_dbgs	.req r8
rx_cpsr .req r7

#---------------- main macro ------------------------------------------------------------------
.macro	debug_print
	stmdb	sp!, {r0-r15}		@ store registers to stack
	mrs	rx_cpsr, cpsr		@ store CPSR to rx_cpsr
	bl	debug_print_main
	msr	cpsr, rx_cpsr		@ restore CPSR from rx_cpsr
	ldmia	sp!, {r0-r12}		@ restore r0~r12
	add	sp, sp, #4		@ ignore restoring sp; set it manually
	ldmia	sp!, {r14}		@ restore lr
	add	sp, sp, #4		@ ignore restoring pc; it doesn't make sense
.endm

debug_print_main:
	mov	r12, lr			@ save LR

	add	r0, sp, #64		@ saved SP is current value(after stmdb); add 4 * 16
	str	r0, [sp, #0x34]		@ fix saved SP value

	ldr	r0, [sp, #0x3C]		@ load saved PC value
	sub	r0, r0, #8		@ saved_PC = real_PC + 8 because of ARM pipeline legacy
	str	r0, [sp, #0x3C]		@ fix saved PC value

	ldr	rx_uart_CH_STS,    =UART_CHANNEL_STS_REG0	@ load UART address
	ldr	rx_uart_TXRX_FIFO, =UART_TX_RX_FIFO0
	ldr	rx_dbgs, =debug_text	@ debug_text pointer
	bl	print_debug_text	@ print debug_text until '\0' reached

#1. print r0-r15
	mov	r4, sp			@ loop start index (= front of stack)
	add	r5, sp, #64		@ loop end index   (= end of stack)
regprint_loop:
	ldr	r0, [r4], #4		@ get register value
	bl	print_register		@ print register to uart
	bl	print_debug_text	@ print debug_text continuously
	cmp	r4, r5			@ check if every register printed
	bne	regprint_loop		@ loop register printing

#2. print NZCV flag
.macro	print_cond_flag	ch, mask
	mov	r0, \ch			@ load flag character (uppercase)
	tst	rx_cpsr, \mask		@ check if flag is set
	addeq	r0, r0, #0x20		@ make lowercase if flag is unset
	bl	putchar			@ print flag character
.endm
	print_cond_flag	#0x4E, #1<<31	@ N flag
	print_cond_flag #0x5A, #1<<30	@ Z flag
	print_cond_flag #0x43, #1<<29	@ C flag
	print_cond_flag #0x56, #1<<28	@ V flag
	bl	print_debug_text

#3. print AIF flag
.macro	print_mask_bits	ch, mask
	mov	r0, \ch			@ load flag character (uppercase)
	tst	rx_cpsr, \mask		@ check if flag is set
	blne	putchar			@ print flag character if flag is set
.endm
	print_mask_bits	#0x41, #1<<8	@ A flag
	print_mask_bits #0x49, #1<<7	@ I flag
	print_mask_bits #0x46, #1<<6	@ F flag
	bl	print_debug_text

#4. print instruction mode (JT)
	ldr	r0, =inst_mode_text	@ string pointer to "ARM" 
	tst	rx_cpsr, #1<<24		@ check if J flag is set
	addne	r0, r0, #10		@ move pointer to "Jazelle" if J is set
	tst	rx_cpsr, #1<<5		@ check if T flag is set
	addne	r0, r0, #4		@ move pointer to "Thumb" if T is flag
	bl	puts			@ print instruction mode string
	bl	print_debug_text

#5. print processor mode (M[4:0])
	ldr	r0, =proc_mode_text
	and	r1, rx_cpsr, #0x0F	@ get M[3:0]; M[4] is always 1
	lsl	r1, r1, #2		@ r1 = M[3:0] * mode_text length (=4)
	add	r0, r0, r1		@ move string pointer to appropriate mode text
	bl	puts			@ print processor mode string
	bl	print_debug_text

#6. print register
	mov	r0, rx_cpsr		@ get cpsr value
	bl	print_register		@ print cpsr value to uart
	bl	print_debug_text

	mov	pc, r12			@ pop call stack


#---------------- sub method ------------------------------------------------------------------
.macro	check_tx_empty	label
	ldr	r3, [rx_uart_CH_STS]	@ get Channel Status Register
	and	r3, r3, #0x8		@ get Transmit Buffer Empty bit
	cmp	r3, #0x8		@ check if TxFIFO is empty and ready to receive new data
	bne	\label			@ if TxFIFO is not empty, check again until it is empty
.endm

putchar:
	check_tx_empty	putchar
	strb	r0, [rx_uart_TXRX_FIFO]	@ fill the TxFIFO
	mov	pc, lr			@ pop call stack


.macro	print_string_method	label, reg
\label :
	check_tx_empty	\label		@ check TxFIFO is empty
	ldrb	r3, [ \reg ], #1	@ get 1 byte from string pointer
	strb	r3, [rx_uart_TXRX_FIFO]	@ fill the TxFIFO
	cmp	r3, #0x00		@ check if r2 is end of menu_text
	bne	\label			@ loop until whole string is printed
	mov	pc, lr			@ pop call stack
.endm

print_string_method  puts, r0
print_string_method  print_debug_text, rx_dbgs


print_register:
	mov	r11, lr			@ save lr
	mov	r2, r0			@ save register value
	mov	r1, #32

hex_l:	sub	r1, r1, #4		@ get shift amount
	lsr	r0, r2, r1		@ shift saved value
	and	r0, r0, #0xF		@ get hex of the register part

	cmp	r0, #10			@ compare r0 with 10
	addlt	r0, r0, #48		@ r0 += '0'      if r0 <  10
	addge	r0, r0, #87		@ r0 += ('a'-10) if r0 >= 10
	bl	putchar			@ print hex to uart

	cmp	r1, #16			@ check if loop is at middle of string
	moveq	r0, #95			@ '_'
	bleq	putchar			@ print '_' to uart

	cmp	r1, #0			@ check if method is finish
	bne	hex_l			@ loop hex printing

	mov	pc, r11			@ pop call stack
