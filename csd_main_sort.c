#pragma GCC target ("thumb")
//#pragma GCC target ("arm")

int indata[32] = {
	  2,   0,  -7,  -1,   3,   8,  -4,  10,  
	 -9, -16,  15,  13,   1,   4,  -3,  14, 
	 -8, -10, -15,   6, -13,  -5,   9,  12, 
	-11, -14,  -6,  11,   5,   7,  -2, -12
};

int outdata[32];

int main() { 
	int i, j, temp;

	for (i = 0; i < 32; ++i)
		outdata[i] = indata[i];

	for (i = 31; i >= 0; --i) {
		for (j = 0; j < i; ++j) {
			if (outdata[j] > outdata[i]) {
				temp = outdata[j];
				outdata[j] = outdata[i];
				outdata[i] = temp;
			}
		}
	}

	return 0;
}
