#include <xtime_l.h>

#define COUNTS_PER_USECOND (COUNTS_PER_SECOND / 1000000)

static XTime interval;
static XTime tCur, tEnd;

void timer_init(void) {
	XTime_GetTime(&tCur);	// initialize tCur = tEnd = getTime()
	interval = ((XTime)1000000) * COUNTS_PER_USECOND;
	tEnd = tCur + interval;
}

int timer_step(void)
{
	XTime_GetTime(&tCur);			// tCur = getTime()
	if (tCur < tEnd) return FALSE;	// return false if interval is not end
	tEnd = tCur + interval;			// update tEnd if interval is end
	return TRUE;
}

int timer_set_interval(unsigned int input) {
	if (input & 0xFFFF0000) return FALSE;			// ignore if input is longer than 1 character
	input = ((input >> 8) & 0xFF);					// get last character of input
	if (input < '1' || '8' < input) return FALSE;	// ignore if input is meaningless
	input = (input != '8' ? input : 'A') - '0';		// convert '1'~'8' to 1~10; then input*100 ms is interval
	interval = ((XTime)input) * 100000 * COUNTS_PER_USECOND;	// update interval using input value
	return TRUE;									// successfully changed interval
}
